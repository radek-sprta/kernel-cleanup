#!/bin/bash
# 
# Removes old kernels, keeping the current one, and a specified 
# number of most recent kernels.  Designed for APT-based distros.  
# This is a modified script from JDPFU.com.
#
# Author:   Radek SPRTA
# Date:     2015/05/05
# License:  Modified BSD 3-clause To see, use option: -l

export PATH=/bin:/usr/bin/

readonly AUTHOR="Radek SPRTA"
readonly CHK_REBOOT_REQ=/var/run/reboot-required
KEEP=1

show_license() {
    printf "
\"Revised BSD License\" (3-clause) for %s :
====== License Start ======
Copyright (c) 2013, $AUTHOR
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the $AUTHOR nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
\"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL $AUTHOR BE 
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  
====== End License ======

" $(basename $0) >&2
   exit 1
}

show_usage() {
  printf "
Options: \n
-h, --help         show this help and exit
-l,                show license and exit
-n NUMBER          specify the number of kernels to keep 
" $(basename $0) >&2
  exit 1
}
   
error() {
  echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@" >&2
}

while getopts 'lhn:' option
do
  case "${option}" in
    l) show_license ;;
    h) show_usage ;;
       # The most recent kernel is included automatically, so we exclude it
    n) let KEEP="${OPTARG}"-1 ;;
    *) show_usage
  esac
done
readonly KEEP

if [[ -f "${CHK_REBOOT_REQ}" ]] ; then
  echo "New kernel installed - reboot needed before cleanup attempts."
  cat "${CHK_REBOOT_REQ}"
  exit 0;
fi

# Determine the current kernel.
readonly cur="linux-image-$(uname --kernel-release)"

# Retrieve a list of kernels
# Clean up the list, removing everything except the package name
# Sort by version
# Leave out the specified number of most recent kernels
readonly raw_list=$(dpkg --list \
                      | grep linux-image-[0-9] \
                      | grep --invert-match "${cur}" \
                      | sed --expression='s/   [0-9].*$//g' --expression='s/^.* l/l/g' \
                      | sort --version-sort \
                      | head --lines=-"${KEEP}")

# Count the number of kernels, and compare it with number to keep
readonly lines=$(echo "${raw_list}" | grep --only-matching 'linux-image' | wc --lines)
if [[ "${lines}" -le "${KEEP}" ]] ; then
   echo "Kernel cleanup not necessary.
${lines} kernels have been retained in addition to the currently used kernel:
${raw_list}
"
   exit 0
fi

list=$(echo ${raw_list} | tr --delete '\n')

# Purge old kernels
sudo apt-get --assume-yes purge ${list} && sudo apt-get --assume-yes autoremove
if [[ "$?" = 0 ]]; then
  echo "The following kernels were deleted:
${list}
"
  exit 0 
else
  error "Could not delete old kernels."
  exit 2
fi
